1. create the project folder called 'task-api'
2. initialize an npm by running command:

	$npm init 

3. create the app entry point file:

	$touch index.js

4. install the packages for express, nodemone and running command:

	$npm install express nodemon mongoose

5. set up the initial codes for creating a server. 

6. open your mongoDB atlas account then create a new DB called 'b125-tasks' with an 
initial collection name called 'tasks'.  

7. get the connection string on the MongoDB Atlas and update its password and the DB name:

	sample connection string :
							//password								//
	mongodb+srv://Shadraque:201557Intp!@cluster0.lwdvy.mongodb.net/b125-tasks?retryWrites=true&w=majority