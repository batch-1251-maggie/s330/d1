const express = require(`express`);
const mongoose = require(`mongoose`); // this code is to be used n our db connection and to create
// of our schema and model for our existing MongoDB atlas collection

const app = express(); //creating a server through the use of app
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//mongoose.connect - is a way to connect our mongodb atlas db connection string to our server
mongoose.connect("mongodb+srv://Shadraque:201557Intp!@cluster0.lwdvy.mongodb.net/b125-tasks?retryWrites=true&w=majority"
	, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
}).then(() => { //if the mongoose succeeded on the connection, then we will 
	//console.log message
	console.log("Successfully Connected to Database!");
}).catch((error)=> { //handles error when the mongoose failed to connect on
	//our mongodb atlas database
	console.log(error);
})

// -------------------------------------------------------------

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: Boolean,
		default: false
	}
});

const Task = mongoose.model('Task', taskSchema);


//CRUD

//insert new task
app.post('/add-task', (req, res) => {
	let newTask = new Task ({
		name: req.body.name
	})

	newTask.save((error, savedTask) => {
		if (error) {
			console.log(error);
		} else {
			res.send(`New task saved! ${savedTask}`);
		}
	});
});

// Retrieve all tasks
app.get(`/retrieve-tasks`, (req, res) => {
	Task.find({}, (error, records) => {
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}
	})
})

// Retrieve tasks that re done, means the status = true
app.get(`/retrieve-tasks-done`, (req, res) => {
	Task.find({status:true}, (error, records) => {
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}
	})
})

//Update operation
app.put(`/complete-task/:taskId`, (req, res) => {
	// res.send({ urlParams: req.params.taskId })});
	let taskId = req.params.taskId;
	Task.findByIdAndUpdate(taskId, {status:true}, (error, updatedTask) => {
		if (error) {
			console.log(error);
		} else {
			res.send(`Task complete successfully!`)
		}
	})
})

//Delete Operation
app.delete('/delete-task/:taskId', (req, res) => {
let taskId = req.params.taskId;
	Task.findByIdAndDelete(taskId, (error, deletedTask) => {
		if (error) {
			console.log(error);
		} else {
			res.send(`Task deleted successfully!`)
		}
	})
})


//USSERRSS!!!

	//mini activity:

const userSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
	userName: String,
	password: String
});

const User = mongoose.model('User', userSchema);

//INSERT/POST
app.post('/register', (req, res) => {
	let newUser = new User ({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		userName: req.body.userName,
		password: req.body.password
	})

	newUser.save((error, savedUser) => {
		if (error) {
			console.log(error);
		} else {
			res.send(`New user saved! ${savedUser}`);
		}
	});
});

//GET/RETRIEVE ALL
app.get(`/retrieve-users`, (req, res) => {
	User.find({}, (error, records) => {
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}
	})
})

////// E X T R A
//Update operation
app.put(`/update-User/:UserId`, (req, res) => {
	// res.send({ urlParams: req.params.UserId })});
	let UserId = req.params.UserId;
	User.findByIdAndUpdate(UserId, {firstName:"Maggie"}, (error, updatedUser) => {
		if (error) {
			console.log(error);
		} else {
			res.send(`User updated successfully!`)
		}
	})
})

//Delete Operation
app.delete('/delete-User/:UserId', (req, res) => {
let UserId = req.params.UserId;
	User.findByIdAndDelete(UserId, (error, deletedUser) => {
		if (error) {
			console.log(error);
		} else {
			res.send(`User deleted successfully!`)
		}
	})
})




app.listen(port, ()=> console.log(`Server is running at port ${port}`))

